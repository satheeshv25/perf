/**
 * Created by skpalani on 17/10/16.
 */
// Type definitions for nodejs-driver v0.8.2
// Project: https://github.com/datastax/nodejs-driver
// Definitions by: Marc Fisher <http://github.com/Svjard>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare module "dse-driver" {
    export interface Client {
        hosts: any;
        graphOptions: any;
        keyspace: string;
        wid: number;
        poolidx: string;
        ws: WebSocket;
        BaseClient: Client;

        executeGraph(script: string, bindings: {}, callback?: Function): void;
        executeGraph(script: string, bindings?: {}, message?: string, callback?: Function): void;

    }

    export module geometry{
        
        interface PointJSON {
            type: string;
            coordinates: number[];
        }

        interface PolygonJSON {
            type: string;
            coordinates: any;

        }
        
        export class Point {
            public x:number;
            public y:number;
            public static fromString(pointWKT: string): Point;
            public toJSON(): PointJSON;
            
        }

        export class Polygon {

            public static fromString(polygonWKT: string): Polygon;

            public toJSON(): PolygonJSON;

        }
    }

    export function Client(options?: { contactPoints: any, keyspace: string, graphOptions?: any }): void;

    

    

}

