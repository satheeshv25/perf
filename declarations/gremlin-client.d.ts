// Type definitions for Gremlin-Client 1.0.3
// Project: https://www.npmjs.com/package/gremlin-client
// Definitions by: Prabakar Kalivaradan <https://github.com/kprabakar>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

// This is just kick-starter definition. Should be improved much more.

declare module "gremlin-client" {

    export interface GremlinClient {
        sessionId: string;
        connected: boolean;
        wid: number;
        poolidx: string;
        ws: WebSocket;
// Events
        on(event: 'error', cb: (err: Error) => void): this;
        on(event: 'close', cb: (code: number, message: string) => void): this;
        on(event: 'message', cb: (data: any, flags: { binary: boolean }) => void): this;
        on(event: 'ping', cb: (data: any, flags: { binary: boolean }) => void): this;
        on(event: 'pong', cb: (data: any, flags: { binary: boolean }) => void): this;
        on(event: 'open', cb: () => void): this;
        on(event: string, listener: () => void): this;

        execute(script: string, bindings?: {}, message?: string, callback?: Function): void;
        execute(script: string, bindings: {}, callback?: Function): void;
        // execute(script: any, fn: Function);
        stream(script: string, bindings?: {}, message?: string): NodeJS.EventEmitter;
    }

// export interface GremFluentScript {
//   V(x?: any): GremFluentScript;
//   order(x: any): GremFluentScript;
// }
    export function createClient(port?: number, host?: string, options?: { session?: boolean }): GremlinClient;

    // export function createClient({language: string}): GremlinClient;
    // export function V(v: any): GremFluentScript;
    // export var g: GremFluentScript;
    // export = g;
}