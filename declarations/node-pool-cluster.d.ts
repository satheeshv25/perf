// Type definitions for compression
// Project: https://github.com/expressjs/compression
// Definitions by: Santi Albo <https://github.com/santialbo/>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference path="generic-pool.d.ts" />
/// <reference path="dse-driver.d.ts" />
/// <reference path="gremlin-client.d.ts" />

declare module "node-pool-cluster" {

    import genericPool = require('generic-pool');
    import gremlin = require('gremlin-client');
    import dse_driver = require('dse-driver');

    namespace e {
        function initCluster(poolOptions: INodePoolClusterOptions<any>): any;

        interface INodePoolCluster<T> {
            addPool(callback: Function): genericPool.Pool<T>;
            removePool(callback: Function): boolean;
            acquire(callback: Function): boolean;
        }

        interface INodePoolClusterOptions<T> {
            /** name of pool */
            name: string;

            /** maximum number of resources to create at any given time (optional, default=1) */
            max?: number;

            /**
             * minimum number of resources to keep in pool at any given time
             * if this is set >= max, the pool will silently set the min to factory.max - 1 (Note: min==max case is expected to change in v3 release)
             * optional (default=0)
             */
            min?: number;
            /** boolean that specifies whether idle resources at or below the min threshold should be destroyed/re-created. optional (default=true) */
            refreshIdle?: boolean;

            /** max milliseconds a resource can go unused before it should be destroyed (default 30000) */
            idleTimeoutMillis?: number;

            /** frequency to check for idle resources (default 1000) */
            reapIntervalMillis?: number;

            /**
             * boolean, if true the most recently released resources will be the first to be allocated.
             * This in effect turns the pool's behaviour from a queue into a stack. optional (default false)
             */
            returnToHead?: boolean;

            /** int between 1 and x - if set, borrowers can specify their relative priority in the queue if no resources are available. (default 1) */
            priorityRange?: number;

            /**
             * function that accepts a pooled resource and returns true if the resource is OK to use, or false if the object is invalid.
             * Invalid objects will be destroyed. This function is called in acquire() before returning a resource from the pool.
             * Optional. Default function always returns true.
             */
            validate?: (gremlinClient: T) => boolean;

            /**
             * Asynchronous validate function.
             * Receives a callback function as its second argument, which should be called with a single boolean argument being true if the item is still valid and false if it should be removed from the pool.
             * Called before item is acquired from pool. Default is undefined.
             * Only one of validate/validateAsync may be specified
             */
            validateAsync?: (gremlinClient: T, callback: (remove: boolean) => void) => void;

            /**
             * If a log is a function, it will be called with two parameters:
             *  - log string
             *  - log level ('verbose', 'info', 'warn', 'error')
             * Else if log is true, verbose log info will be sent to console.log()
             * Else internal log messages be ignored (this is the default)
             */
            log?: boolean | ((log: string, level: 'verbose' | 'info' | 'warn' | 'error') => void);

            // destroy?: (gremlinClient: gremlin.GremlinClient) => void;

            destroy(gremlinClient: T): void;

        }
    }
    export = e;
}
