#!/usr/bin/env node
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Express = require("express");
const compression = require("compression");
const bodyParser = require("body-parser");
const routers = require("./router");
const Bunyan_1 = require("./logger/Bunyan");
class ExpressApp {
    constructor() {
        this._app = Express();
        this._app.set('etag', false);
        this._app.use(bodyParser.json());
        this._app.use(compression({ filter: this.shouldCompress }));
        this._app.use(this.errorHandler);
        this._app.use(this.clientErrorHandler);
        var env = process.env.NODE_ENV || 'development';
        if ('development' == env) {
            ExpressApp.log.debug("Express running in [Development Mode]");
        }
        else {
            ExpressApp.log.debug("Express running in [Production Mode]");
        }
        new routers.Routers(this._app);
    }
    shouldCompress(req, res) {
        let compressionHeader = req.headers['x-compression'];
        if (!(compressionHeader === null)) {
            return (compressionHeader === "true");
        }
        return compression.filter(req, res);
    }
    clientErrorHandler(err, req, res, next) {
        ExpressApp.log.debug("clientErrorHandler : [" + JSON.stringify(err) + "]");
        if (err && !req.body[0]) {
            ExpressApp.log.error(err);
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.write(JSON.stringify({
                statusCode: 400,
                statusDescr: "Bad Request",
                details: err
            }));
            res.end();
        }
        else {
            next(err);
        }
    }
    errorHandler(err, req, res, next) {
        ExpressApp.log.debug("errorHandler : [" + JSON.stringify(err) + "]");
        if (err.SyntaxError) {
            ExpressApp.log.error(err);
            res.writeHead(400, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({ "Error": "Bad Request1" }));
        }
        else {
            next(err);
        }
    }
    get app() {
        return this._app;
    }
}
ExpressApp.log = new Bunyan_1.Bunyan().log;
exports.ExpressApp = ExpressApp;
