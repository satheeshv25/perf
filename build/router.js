#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Express = require("express");
const execute = require("./queryExecutor/execute");
class Routers {
    static global(req, res, next) {
        res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
        res.header('Access-Control-Allow-Headers', 'access-control-allow-origin, x-compression,Authorization, Origin, X-Requested-With, Content-Type, Accept');
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
        res.header('Pragma', 'no-cache');
        res.header('Expires', 0);
        next();
    }
    constructor(expressApp) {
        this._versionRouter = Express.Router();
        this._versionRouter.use(Routers.global);
        new execute.ExecuteRouter(this._versionRouter);
        expressApp.use('/', this._versionRouter);
    }
}
exports.Routers = Routers;
