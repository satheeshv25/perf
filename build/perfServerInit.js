#!/usr/bin/env node
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Http = require("http");
const DEBUG = require("debug");
const myExpress = require("./ExpressApp");
const Bunyan_1 = require("./logger/Bunyan");
const hostProperties_1 = require("./hostProperties");
const DSEConnectionPool_1 = require("./db/DSEConnectionPool");
const EsUtil_1 = require("./db/EsUtil");
exports.PORT = process.env.PORT || hostProperties_1.properties.server_port;
class CompositeServer {
    constructor() {
        this._expressApp = new myExpress.ExpressApp().app;
    }
    start() {
        CompositeServer.log.debug(`going to start server here.... on ${exports.PORT}`);
        this._server = Http.createServer(this._expressApp);
        this._server.listen(exports.PORT);
        this._server.on('error', error => {
            throw error;
        });
        this._server.on('listening', () => {
            let addr = this._server.address();
            let bind = typeof addr === 'string'
                ? 'pipe ' + addr
                : 'port ' + addr.port;
            DEBUG('Listening on ' + bind);
            DEBUG('Creating Connection Pool ');
            new EsUtil_1.EsUtil();
            DSEConnectionPool_1.DSEConnectionPool.init();
        });
    }
}
CompositeServer.log = new Bunyan_1.Bunyan().log;
exports.CompositeServer = CompositeServer;
