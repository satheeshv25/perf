#!/usr/bin/env node
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Server = require("./perfServerInit");
const Bunyan_1 = require("./logger/Bunyan");
const log = new Bunyan_1.Bunyan().log;
new Server.CompositeServer().start();
log.debug('Server is running on port', Server.PORT);
