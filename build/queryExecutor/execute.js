#!/usr/bin/env node
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const ResponseWriter_1 = require("../util/ResponseWriter");
const async = require("async");
const Bunyan_1 = require("../logger/Bunyan");
const DbServiceUtil_1 = require("../db/DbServiceUtil");
class ExecuteRouter {
    constructor(router) {
        this._router = router;
        this._router.post('/execute', this.httpPost);
    }
    httpPost(req, res, next) {
        const write2Browser = function (err, result) {
            if (!err) {
                ResponseWriter_1.resultWriter(req, res, next, result);
            }
            else {
                ResponseWriter_1.errorWriter(req, res, next, err);
            }
            res.end();
        };
        const query = "g.V().hasLabel('VEHICLES').has('accountTId','a976cec3-27d2-4082-bd04-f04c196c4b24').has('objCat','VEHICLES').has('lifeState',without('DELETED')).values('_id').fold()";
        const params = { "username": "manageacc10@mailinator.com", "permissions": ["R", "U", "D", "A"], "currentDt": "2018-06-10T17:56:25.678Z" };
        async.autoInject({
            dbConnect: function (callback) {
                DbServiceUtil_1.DbServiceUtil.dseQueryExecution(query, null, callback);
            },
            data: ['dbConnect', function (results, callback) {
                    results.forEach(function (element) {
                        callback(null, element);
                    });
                }]
        }, function (err, results) {
            if (!err) {
                write2Browser(null, results.data);
            }
            else {
                write2Browser(["503", "Service Unavailable", err], null);
                ExecuteRouter.log.error("Service Unavailable", err);
            }
        });
    }
}
ExecuteRouter.log = new Bunyan_1.Bunyan().log;
exports.ExecuteRouter = ExecuteRouter;
