"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bunyan = require('bunyan');
const hostProperties_1 = require("../hostProperties");
class Bunyan {
    constructor() {
        this.log = bunyan.createLogger({
            name: "perf Logger",
            streams: hostProperties_1.properties.logger
        });
    }
}
exports.Bunyan = Bunyan;
