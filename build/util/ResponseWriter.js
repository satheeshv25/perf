"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util = require("./Util");
function resultWriter(req, res, next, result) {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(util.filteredJsonStringify(result, util.API_EXCLUDE_INTERNAL_PROPERTIES));
}
exports.resultWriter = resultWriter;
function errorWriter(req, res, next, err) {
    res.writeHead(err[0], { 'Content-Type': 'application/json' });
    if (err[2]) {
        res.write(JSON.stringify({
            statusCode: err[0],
            statusDescr: err[1],
            details: { statusCode: err[0], statusDescr: err[2] }
        }));
    }
    else {
        res.write(JSON.stringify({ statusCode: err[0], statusDescr: err[1] }));
    }
}
exports.errorWriter = errorWriter;
