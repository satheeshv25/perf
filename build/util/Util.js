"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Collections = require("typescript-collections");
const util = require("util");
const Bunyan_1 = require("../logger/Bunyan");
const log = new Bunyan_1.Bunyan().log;
exports.API_EXCLUDE_INTERNAL_PROPERTIES = function () {
    let excludeProps = new Collections.Set();
    excludeProps.add('personIds');
    excludeProps.add('vehicleIds');
    excludeProps.add('placeIds');
    excludeProps.add('sensorIds');
    return excludeProps;
}();
function printObject(message, obj, keys) {
    var toPrint = {};
    if (keys != null) {
        keys.forEach(function (key) {
            let value = getObject(obj, key);
            if (value !== null)
                toPrint[key] = value;
        });
    }
    else {
        toPrint = obj;
    }
    var printOptions = {
        showHidden: true,
        depth: 2,
        colors: true
    };
    log.debug(message + ": [" + util.inspect(toPrint, printOptions).replace('\n', '') + "]");
}
exports.printObject = printObject;
function getObject(theObject, key) {
    var result = null;
    if (theObject instanceof Array) {
        for (var i = 0; i < theObject.length; i++) {
            result = getObject(theObject[i], key);
            if (result) {
                break;
            }
        }
    }
    else {
        for (var prop in theObject) {
            if (theObject.hasOwnProperty(prop)) {
                if (prop.startsWith("_") || get_type(theObject[prop]) === "[object Function]")
                    continue;
                if (prop === key) {
                    return theObject[prop];
                }
                if (theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
                    result = getObject(theObject[prop], key);
                    if (result) {
                        break;
                    }
                }
            }
        }
    }
    return result;
}
exports.getObject = getObject;
function get_type(thing) {
    if (thing === null)
        return "[object Null]";
    return Object.prototype.toString.call(thing);
}
exports.get_type = get_type;
function getAsNumber(numberString) {
    let number = 0;
    try {
        number = parseInt(numberString);
    }
    catch (e) {
    }
    return number;
}
exports.getAsNumber = getAsNumber;
function filteredJsonStringify(data, excludeProperties) {
    let filterFn = function (key, value) {
        if (excludeProperties.contains(key)) {
            return undefined;
        }
        return value;
    }.bind(this);
    return JSON.stringify(data, filterFn);
}
exports.filteredJsonStringify = filteredJsonStringify;
function getFieldsSplittedFromString(stringToBeSplit, splitChar) {
    let splittedStringArray = stringToBeSplit.toString().split(splitChar);
    return splittedStringArray;
}
exports.getFieldsSplittedFromString = getFieldsSplittedFromString;
function containsString(fullstring, substring) {
    let contains = false;
    if (!fullstring || !substring)
        return false;
    let index = fullstring.indexOf(substring);
    if (index > -1)
        contains = true;
    else
        contains = false;
    return contains;
}
exports.containsString = containsString;
function arrayContainsKey(array, key) {
    var regEx = new RegExp(key);
    for (var i = 0; i < array.length; i++) {
        if (regEx.test(array[i]))
            return true;
    }
}
exports.arrayContainsKey = arrayContainsKey;
class FsmTimeTracker {
    static startNewTracker(name) {
        let tracker = new FsmTimeTracker();
        tracker.startTime = Date.now();
        tracker.currentMark = tracker.startTime;
        tracker.name = name;
        log.debug(`##### Starting ${name} timer #####`);
        return tracker;
    }
    discreteMeasure(taskName) {
        let nowTime = Date.now();
        let diff = nowTime - this.currentMark;
        this.currentMark = nowTime;
        log.debug(`#####${this.name}: ${taskName} took ${diff} milliseconds (discrete) #####`);
        return diff;
    }
    cumulativeMeasure(taskName) {
        let nowTime = Date.now();
        let diff = nowTime - this.startTime;
        log.debug(`#####${this.name}: ${taskName} took ${diff} milliseconds so far (cumulative) #####`);
        return diff;
    }
    startDiscreteNow() {
        this.currentMark = Date.now();
    }
}
exports.FsmTimeTracker = FsmTimeTracker;
