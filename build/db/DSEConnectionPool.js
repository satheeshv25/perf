#!/usr/bin/env node
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const DSEDriver = require("dse-driver");
const dseGraph = require("dse-graph");
class DSEConnectionPool {
    static init() {
        DSEConnectionPool.g = dseGraph.traversalSource(DSEConnectionPool.client);
        DSEConnectionPool.client = new DSEDriver.Client({
            contactPoints: ['10.210.243.22'],
            profiles: [
                new DSEDriver.ExecutionProfile('default', { graphOptions: { name: 'OCULUS' } })
            ]
        });
    }
}
exports.DSEConnectionPool = DSEConnectionPool;
