#!/usr/bin/env node
'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const DSEConnectionPool_1 = require("./DSEConnectionPool");
const EsUtil_1 = require("./EsUtil");
const Bunyan_1 = require("../logger/Bunyan");
const Util_1 = require("../util/Util");
class DbServiceUtil {
    constructor() {
    }
    static dseQueryExecution(dseQuery, dseParam, callback) {
        DbServiceUtil.log.debug('started dseQueryExecution');
        const timer = Util_1.FsmTimeTracker.startNewTracker('connection Acquire');
        function detData(dseQuery, dseParam, callback) {
            return __awaiter(this, void 0, void 0, function* () {
                const result = yield DSEConnectionPool_1.DSEConnectionPool.client.executeGraph(dseQuery, dseParam);
                callback(null, result);
            });
        }
    }
    static esClient(esType, esQuery, callback) {
        esType === "awsEs" ? EsUtil_1.EsUtil.searchAWS(esQuery, callback) : EsUtil_1.EsUtil.searchEs(esQuery, callback);
    }
}
DbServiceUtil.log = new Bunyan_1.Bunyan().log;
exports.DbServiceUtil = DbServiceUtil;
