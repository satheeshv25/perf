#!/usr/bin/env node
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var elasticsearch = require('elasticsearch');
var AWS = require('aws-sdk');
const hostProperties_1 = require("../hostProperties");
const Bunyan_1 = require("../logger/Bunyan");
class EsUtil {
    constructor() {
        EsUtil.esClient = new elasticsearch.Client({
            host: hostProperties_1.properties.es_metrics[0].host + ':' + hostProperties_1.properties.es_metrics[0].port,
            log: hostProperties_1.properties.es_metrics[0].logConfig
        });
        EsUtil.awsEnvironmentalCredentials = new AWS.EC2MetadataCredentials();
        EsUtil.awsEsClient = elasticsearch.Client({
            hosts: hostProperties_1.properties.aws_es[0].hosts,
            connectionClass: require('http-aws-es'),
            amazonES: {
                region: hostProperties_1.properties.aws_es[0].region,
                credentials: EsUtil.awsEnvironmentalCredentials
            }
        });
    }
    static refreshAwsCreds(client, awsCreds) {
        if (!awsCreds.needsRefresh())
            return Promise.resolve(null);
        return new Promise((resolve, reject) => {
            awsCreds.refresh((err) => {
                if (err)
                    return reject(err);
                client.creds = awsCreds;
                resolve(null);
                EsUtil.log.debug('Refreshed AWS Credentials', EsUtil.awsEsClient.creds);
            });
        });
    }
    static searchEs(esQuery, callback) {
        EsUtil.esClient.search(esQuery).then(function (body) {
            EsUtil.log.debug("Elastic Search Query Executed");
            callback(null, body);
        }, function (error) {
            EsUtil.log.error("Error in Elastic Search Query execution failed", error.message);
            callback(error, null);
        });
    }
    static searchAWS(esQuery, callback) {
        this.refreshAwsCreds(EsUtil.awsEsClient, EsUtil.awsEnvironmentalCredentials)
            .then(() => {
            EsUtil.awsEsClient.search(esQuery).then(function (body) {
                EsUtil.log.debug("Elastic Search Query Executed");
                callback(null, body);
            }, function (error) {
                EsUtil.log.error("Service Unavailable, Error in Elastic Search Query execution failed", error.message);
                callback(error, null);
            });
        })
            .catch((error) => {
            EsUtil.log.error("Serivce Unavailable, Error in Elastic Search Query execution", error.message);
            callback(error, null);
        });
    }
}
EsUtil.log = new Bunyan_1.Bunyan().log;
exports.EsUtil = EsUtil;
