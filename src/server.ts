#!/usr/bin/env node

/// <reference path='../declarations/node.d.ts' />

'use strict';

import Server = require('./perfServerInit');
import {Bunyan} from "./logger/Bunyan";

const log = new Bunyan().log;
new Server.CompositeServer().start();
log.debug('Server is running on port', Server.PORT);
