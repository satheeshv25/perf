#!/usr/bin/env node

/// <reference path='../declarations/node.d.ts' />
/// <reference path='../declarations/express.d.ts' />
/// <reference path="../declarations/express-serve-static-core.d.ts" />
/// <reference path='../declarations/compression.d.ts' />
/// <reference path='../declarations/body-parser.d.ts' />
/// <reference path='../declarations/method-override.d.ts' />

'use strict';

import Express = require('express');
import compression = require('compression');
import bodyParser = require('body-parser');
import methodOverride = require('method-override');
import routers = require('./router');
import util = require('util');
import {Bunyan} from "./logger/Bunyan";

export class ExpressApp {
    private _app = Express();
    public static log = new Bunyan().log;

    constructor() {
        this._app.set('etag', false);
        this._app.use(bodyParser.json()); // support json encoded bodies
        this._app.use(compression({filter: this.shouldCompress}));
        this._app.use(this.errorHandler);
        this._app.use(this.clientErrorHandler);

        var env = process.env.NODE_ENV || 'development';
        if ('development' == env) {
            ExpressApp.log.debug("Express running in [Development Mode]");
        } else {
            ExpressApp.log.debug("Express running in [Production Mode]");
        }

        new routers.Routers(this._app);
    }

    public shouldCompress(req, res): boolean {
        let compressionHeader = req.headers['x-compression'];
        if (!(compressionHeader === null)) { // compress responses only with this request header
            return (compressionHeader === "true"); // and set to True
        }
        return compression.filter(req, res); // fallback to standard filter function
    }

    public clientErrorHandler(err, req, res, next) {
        ExpressApp.log.debug("clientErrorHandler : [" + JSON.stringify(err) + "]");
        if (err && !req.body[0]) { // There is an Error and Body is Empty
          ExpressApp.log.error(err);
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                statusCode : 400,
                statusDescr: "Bad Request",
                details    : err
            }));
            res.end();
        } else {
            next(err);
        }
    }

    public errorHandler(err, req, res, next) {
        ExpressApp.log.debug("errorHandler : [" + JSON.stringify(err) + "]");
        if (err.SyntaxError) {
            ExpressApp.log.error(err);
            res.writeHead(400, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({"Error": "Bad Request1"}));
        } else {
            next(err);
        }
    }

    get app() {
        return this._app;
    }

}
