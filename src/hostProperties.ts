/// <reference path="../declarations/node.d.ts" />
export let properties =
    {
        server_port: 8080,
        db: [{ host: "10.210.243.21", port: 9042 }],
        dse: [
            { host: "10.210.243.22", port: 9042 }
        ],
        es: [
            {

                host: "10.2410.242.36",
                port: 9200,
                logConfig: 'error'
            }
        ],
        es_metrics: [
            {
                host: "10.210.243.40",
                port: 9200,
                logConfig: 'error'
            }],
        logger: [
            {
                level: "warn",
                stream: process.stderr,
            },
            {
                level: "debug",
                stream: process.stdout
            }
        ],
        aws_es: [
            {
                hosts: "https://search-es-state-dev-f3gbubcjmljmyuq3bcpi67hcnm.us-west-2.es.amazonaws.com/",
                region: "us-west-2"
            }
        ]

    }
