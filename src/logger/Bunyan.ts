const bunyan = require('bunyan');
import {properties} from "../hostProperties";

export class Bunyan {
  public log;
  constructor() {
    this.log = bunyan.createLogger({
        name: "perf Logger",
        streams: properties.logger
    });
  }
}
