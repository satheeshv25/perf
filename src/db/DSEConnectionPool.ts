#!/usr/bin/env node

/**
 * Created by skpalani on 17/5/16.
 */

'use strict';

import Express = require('express');
import DSEDriver = require('dse-driver');
import async = require('async');
import Collections = require('typescript-collections');
import { INodePoolCluster } from "node-pool-cluster";
import nodePoolCluster = require('node-pool-cluster');
import util = require('util');
import { properties } from "../hostProperties";
import utility = require('../util/Util');
import { Bunyan } from "../logger/Bunyan";

export class DSEConnectionPool {
    static connectionsInUse: number = 0;
    static connectionPool = new Collections.Dictionary<string, DSEDriver.Client>();
    public static log = new Bunyan().log;
    public static dseGremlinClient;

    static cluster_pool: INodePoolCluster<DSEDriver.Client> = nodePoolCluster.initCluster({
        name: 'dse-connection-pool',
        max: 10,
        min: 5,
        idleTimeoutMillis: 30000,
        reapIntervalMillis: 30000,
        destroy: function (client) {
            client.shutdown();
        },
        validate: function (connection) {
            DSEConnectionPool.log.debug('Validating Connection');
            utility.printObject(new Date().toISOString() + " : Validate Gremlin Client ", connection, ["connected", "address", "cassandraVersion"]);
            connection.executeGraph("Gremlin.version", null, function (err, results) {
                if (!err) {
                    connection["connected"] = true;
                } else {
                    connection["connected"] = false;
                }
            });

            if (connection["connected"] === true) return true;
            else return false;
        },
        log: false
    });

    public static init() {
        properties.dse.forEach(function (instance) {
            DSEConnectionPool.cluster_pool.addPool(function (callback) {
                let dseGremlinClient = new DSEDriver.Client({
                    contactPoints: [instance.host],
                    keyspace: 'OCULUS',
                    graphOptions: { name: 'OCULUS' }
                });

                dseGremlinClient.executeGraph("Gremlin.version", null, function (err, results) {
                    if (!err) {
                        dseGremlinClient.wid = process.pid;
                        dseGremlinClient.poolidx = process.pid + "-" + (++DSEConnectionPool.connectionsInUse);
                        utility.printObject(new Date().toISOString() + " : Adding DSE Connection : ", dseGremlinClient, ["poolidx", "connected", "address", "cassandraVersion"]);
                        let gremlinSessionId = dseGremlinClient["sessionId"];
                        DSEConnectionPool.connectionPool.setValue(gremlinSessionId, dseGremlinClient);
                        callback(null, dseGremlinClient);
                    } else {
                        callback(err, null);
                    }
                })
            });

            process.on('uncaughtException', function (err) {
                DSEConnectionPool.log.error("Error ", err);
                utility.printObject("uncaughtException err", err, ["message", "details"]);
            });
        });

    }
}
