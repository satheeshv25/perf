#!/usr/bin/env node
'use strict';

import { DSEConnectionPool } from "./DSEConnectionPool";
import { EsUtil } from "./EsUtil";
import { Bunyan } from "../logger/Bunyan";
import Promise from "ts-promise";
import { FsmTimeTracker } from "../util/Util";

export class DbServiceUtil {

    public static log = new Bunyan().log;

    constructor() {
    }

    public static dseQueryExecution(dseQuery, dseParam, callback): void {
        DbServiceUtil.log.debug('started dseQueryExecution');
        const timer: FsmTimeTracker = FsmTimeTracker.startNewTracker('connection Acquire');
        DSEConnectionPool.cluster_pool.acquire(function (err, connection, pool) {
            connection.executeGraph(dseQuery, dseParam, function (err, results) {
                pool.release(connection);
                timer.cumulativeMeasure("Query Execution");
                if (!err) {
                    callback(null, results);
                } else {
                    DbServiceUtil.log.error("Service Unavailable, Error in Dse Query Execution " + err);
                    callback(err, null);
                }
            });
        });

    }

    public static esClient(esType, esQuery, callback): void {
        esType === "awsEs" ? EsUtil.searchAWS(esQuery, callback) : EsUtil.searchEs(esQuery, callback);
    }
}
