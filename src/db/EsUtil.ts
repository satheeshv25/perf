#!/usr/bin/env node

/// <reference path='../../declarations/elasticsearch.d.ts' />

'use strict';

var elasticsearch = require('elasticsearch');
var AWS = require('aws-sdk');
import { properties } from "../hostProperties";
import { Bunyan } from "../logger/Bunyan";
/*
 This is a singleton class and maintains a single ESClient instance.
 ESClient library internally maintains a connection pool which we do not have to worry about.
 */
export class EsUtil {

    private static esClient;
    private static awsEsClient;
    private static awsEnvironmentalCredentials;
    private static log = new Bunyan().log;

    constructor() {
        EsUtil.esClient = new elasticsearch.Client({
            host: properties.es_metrics[0].host + ':' + properties.es_metrics[0].port,
            log: properties.es_metrics[0].logConfig
        });

        EsUtil.awsEnvironmentalCredentials = new AWS.EC2MetadataCredentials();

        EsUtil.awsEsClient = elasticsearch.Client({
            hosts: properties.aws_es[0].hosts,
            connectionClass: require('http-aws-es'),
            amazonES: {
                region: properties.aws_es[0].region,
                credentials: EsUtil.awsEnvironmentalCredentials
            }
        });
    }

    private static refreshAwsCreds(client, awsCreds) {
        if (!awsCreds.needsRefresh()) return Promise.resolve(null);
        return new Promise((resolve, reject) => {
            awsCreds.refresh((err) => {
                if (err) return reject(err);
                client.creds = awsCreds;
                resolve(null);
                EsUtil.log.debug('Refreshed AWS Credentials', EsUtil.awsEsClient.creds);
            });
        });
    }

    public static searchEs(esQuery, callback): void {
        EsUtil.esClient.search(esQuery).then(function (body) {
            EsUtil.log.debug("Elastic Search Query Executed");
            callback(null, body);
        }, function (error) {
            EsUtil.log.error("Error in Elastic Search Query execution failed", error.message);
            callback(error, null);
        });
    }

    public static searchAWS(esQuery, callback): void {
        this.refreshAwsCreds(EsUtil.awsEsClient, EsUtil.awsEnvironmentalCredentials)
            .then(() => {
                EsUtil.awsEsClient.search(esQuery).then(function (body) {
                    EsUtil.log.debug("Elastic Search Query Executed");
                    callback(null, body);
                }, function (error) {
                    EsUtil.log.error("Service Unavailable, Error in Elastic Search Query execution failed", error.message);
                    callback(error, null);
                });
            })
            .catch((error) => {
                EsUtil.log.error("Serivce Unavailable, Error in Elastic Search Query execution", error.message);
                callback(error, null);
            });
    }
}
