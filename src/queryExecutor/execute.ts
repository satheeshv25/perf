#!/usr/bin/env node

/// <reference path='../../declarations/node.d.ts' />
/// <reference path='../../declarations/express.d.ts' />

'use strict';

import Express = require('express');
import { resultWriter, errorWriter } from "../util/ResponseWriter";
import async = require('async');
import { Bunyan } from "../logger/Bunyan";
import { DbServiceUtil } from "../db/DbServiceUtil";
import { DSEConnectionPool } from "../db/DSEConnectionPool";

export class ExecuteRouter {

    private _router;
    public static log = new Bunyan().log;
    constructor(router: Express.Router) {
        this._router = router;
        this._router.post('/execute', this.httpPost);
    }

    httpPost(req: Express.Request, res: Express.Response, next: Express.NextFunction): void {
        const write2Browser: any = function (err, result): void {
            if (!err) {
                resultWriter(req, res, next, result);
            } else {
                errorWriter(req, res, next, err);
            }
            res.end();
        };

        const query: string = "g.V().hasLabel('VEHICLES').has('accountTId','a976cec3-27d2-4082-bd04-f04c196c4b24').has('objCat','VEHICLES').has('lifeState',without('DELETED')).values('_id').fold()";
        const params: object = {"username":"manageacc10@mailinator.com","permissions":["R","U","D","A"],"currentDt":"2018-06-10T17:56:25.678Z"};
        async.autoInject({
            dbConnect: function (callback) {
                DbServiceUtil.dseQueryExecution(query, null, callback);
            },
            data: ['dbConnect',function(results,callback) {
                results.forEach(function(element) {
                    callback(null,element);
                });
            }]
        }, function (err, results) {
            if (!err) {
                write2Browser(null, results.data);
            } else {
                write2Browser(["503", "Service Unavailable", err], null);
                ExecuteRouter.log.error("Service Unavailable", err);
            }
        });
    }
}
