#!/usr/bin/env node

/// <reference path='../declarations/node.d.ts' />
/// <reference path='../declarations/express.d.ts' />
/// <reference path='../declarations/debug.d.ts' />

'use strict';

import Express = require('express');
import Http = require('http');
import DEBUG = require('debug');
import FS = require('fs');
import util = require('util');
import myExpress = require('./ExpressApp');
import {Bunyan} from "./logger/Bunyan";
import {properties} from "./hostProperties";
import {DSEConnectionPool} from "./db/DSEConnectionPool";
import {EsUtil} from "./db/EsUtil";

export const PORT: number = process.env.PORT || properties.server_port;

export class CompositeServer {

    public static log = new Bunyan().log;
    private _server: Http.Server;
    private _expressApp: any = new myExpress.ExpressApp().app;

    constructor() {
    }
    
    start(): void {

        CompositeServer.log.debug(`going to start server here.... on ${PORT}`);
        this._server = Http.createServer(this._expressApp);
        this._server.listen(PORT);
        this._server.on('error', error => {
            throw error;
        });

        this._server.on('listening', () => {
            let addr = this._server.address();
            let bind = typeof addr === 'string'
                ? 'pipe ' + addr
                : 'port ' + addr.port;
            DEBUG('Listening on ' + bind);
            DEBUG('Creating Connection Pool ');
            new EsUtil();
            DSEConnectionPool.init();
        });
    }
}
