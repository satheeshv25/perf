import Express = require('express');
import util = require('./Util');

export function resultWriter(req: Express.Request, res: Express.Response, next: Express.NextFunction, result: any): void {

    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(util.filteredJsonStringify(result, util.API_EXCLUDE_INTERNAL_PROPERTIES));
}

export function errorWriter(req: Express.Request, res: Express.Response, next: Express.NextFunction, err: any): void {

    res.writeHead(err[0], { 'Content-Type': 'application/json' });
    if (err[2]) {
        res.write(JSON.stringify({
            statusCode: err[0],
            statusDescr: err[1],
            details: { statusCode: err[0], statusDescr: err[2] }
        }));
    } else {
        res.write(JSON.stringify({ statusCode: err[0], statusDescr: err[1] }));
    }
}