import Collections = require("typescript-collections");
import util = require('util');
import { Bunyan } from "../logger/Bunyan";

const log = new Bunyan().log;
export type endResponse = () => void;
export type write2Browser = (err, results) => void;

export let API_EXCLUDE_INTERNAL_PROPERTIES: Collections.Set<string> = function (): Collections.Set<string> {
    let excludeProps: Collections.Set<string> = new Collections.Set<string>();
    excludeProps.add('personIds');
    excludeProps.add('vehicleIds');
    excludeProps.add('placeIds');
    excludeProps.add('sensorIds');
    return excludeProps;
}();

export function printObject(message: string, obj: any, keys: string[]) {
    var toPrint = {};
    if (keys != null) {
        keys.forEach(function (key) {
            let value = getObject(obj, key);
            if (value !== null) toPrint[key] = value;
        });
    } else {
        toPrint = obj;
    }
    var printOptions = {
        showHidden: true,
        depth: 2,
        colors: true
    };
    log.debug(message + ": [" + util.inspect(toPrint, printOptions).replace('\n', '') + "]");
}

export function getObject(theObject, key) {
    var result = null;
    if (theObject instanceof Array) {
        for (var i = 0; i < theObject.length; i++) {
            result = getObject(theObject[i], key);
            if (result) {
                break;
            }
        }
    }
    else {
        for (var prop in theObject) {
            if (theObject.hasOwnProperty(prop)) {
                if (prop.startsWith("_") || get_type(theObject[prop]) === "[object Function]") continue;
                if (prop === key) {
                    return theObject[prop];
                }
                if (theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
                    result = getObject(theObject[prop], key);
                    if (result) {
                        break;
                    }
                }
            }
        }
    }
    return result;
}

export function get_type(thing) {
    if (thing === null) return "[object Null]"; // special case
    return Object.prototype.toString.call(thing);
}

export function getAsNumber(numberString): number {
    let number = 0;
    try {
        number = parseInt(numberString);
    } catch (e) {

    }
    return number;
}

export function filteredJsonStringify(data: any, excludeProperties: Collections.Set<string>): string {
    let filterFn = function (key, value) {
        if (excludeProperties.contains(key)) {
            return undefined;
        }
        return value;
    }.bind(this);
    return JSON.stringify(data, filterFn);
}

export function getFieldsSplittedFromString(stringToBeSplit: string, splitChar: string): string[] {
    let splittedStringArray: string[] = <string[]>stringToBeSplit.toString().split(splitChar);
    return splittedStringArray;
}

export function containsString(fullstring: string, substring: string): boolean {
    let contains: boolean = false;
    if (!fullstring || !substring) return false;
    let index: number = fullstring.indexOf(substring);
    if (index > -1)
        contains = true;
    else
        contains = false;
    return contains;

}

export function arrayContainsKey(array: string[], key: string) {
    var regEx = new RegExp(key);
    for (var i = 0; i < array.length; i++) {
        if (regEx.test(array[i]))
            return true;
    }
}

export class FsmTimeTracker {
    private startTime: number;
    private currentMark: number;
    private name: string;

    static startNewTracker(name: string): FsmTimeTracker {
        let tracker = new FsmTimeTracker();
        tracker.startTime = Date.now();
        tracker.currentMark = tracker.startTime;
        tracker.name = name;
        log.debug(`##### Starting ${name} timer #####`);
        return tracker;
    }

    discreteMeasure(taskName: string): number {
        let nowTime = Date.now();
        let diff = nowTime - this.currentMark;
        this.currentMark = nowTime;
        log.debug(`#####${this.name}: ${taskName} took ${diff} milliseconds (discrete) #####`);
        return diff;
    }

    cumulativeMeasure(taskName: string): number {
        let nowTime = Date.now();
        let diff = nowTime - this.startTime;
        log.debug(`#####${this.name}: ${taskName} took ${diff} milliseconds so far (cumulative) #####`);
        return diff;
    }

    startDiscreteNow() {
        this.currentMark = Date.now();
    }
}
